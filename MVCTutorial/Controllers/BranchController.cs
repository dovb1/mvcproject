﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTutorial.Models;
using MVCTutorial.Dal;
using MVCTutorial.ViewModel;
using System.Web.Security;
using System.Threading;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MVCTutorial.Controllers
{
    public class BranchController : Controller
    {
        // GET: Branch
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult ShowMap()
        {
            Session["PageTitle"] = "Our Branches";
            return View();
        }

        public JsonResult getLocations()
        {
            DataLayer Pdal = new DataLayer();
            List<Location> objLocations = Pdal.Locations.ToList<Location>();

            JavaScriptSerializer x = new JavaScriptSerializer();
            string JSONString = x.Serialize(objLocations);
            return Json(JSONString, JsonRequestBehavior.AllowGet);
        }
    }
}
