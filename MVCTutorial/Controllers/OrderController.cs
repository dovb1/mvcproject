﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTutorial.Models;
using MVCTutorial.Dal;
using MVCTutorial.ViewModel;
using System.Web.Security;
using System.Threading;
using System.Data.Entity.Validation;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace MVCTutorial.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ViewResult Index()
        {
            return View();
        }

        // web service!!
        public async Task<JsonResult> GetCurrentDollarJson(string currCoin)
        {
            try {
                HttpClient client = new HttpClient();
                var responseString = await client.GetStringAsync("https://api.exchangeratesapi.io/latest?symbols=" + currCoin);
                return Json(responseString, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult ShowOrders()//return view of show orders page
        {
            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                Session["PageTitle"] = "Orders";
                return View();
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        private IEnumerable<OrderProductViewModel> GetOrderedProducts()
        {
            DataLayer Pdal = new DataLayer();
            List<Order> objOrders = Pdal.Orders.ToList<Order>();
            List<Product> objProducts = Pdal.Products.ToList<Product>();
            var orderProductViewModel = from product in objProducts
                                        join order in objOrders
                                        on product.productID equals order.productID
                                        into result
                                        from newOrder in result.DefaultIfEmpty()
                                        select new OrderProductViewModel { product = product, order = newOrder };
            return orderProductViewModel;
        }
        public JsonResult GetOrderByJson()//return json with all the current orders
        {
            var orderedProducts = GetOrderedProducts();
            return Json(orderedProducts, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowReservationPage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null)
            {
                String data = HttpContext.Request.Cookies.Get("Supplier").Value;
                User user = new User();
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                Session["PageTitle"] = "New Order";
                Session["LogginInUser"] = user.name;
                return View(user);
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }


        }
        public JsonResult MakeOrder()
        {
            Order o = new Order();
            DataLayer Pdal = new DataLayer();
            o.productID = Request.Form["txtProductID"];//Taking information from the form
            o.supplierID = Request.Form["txtUserID"];
            o.amount = Int32.Parse(Request.Form["txtOption"]);
            o.orderDate = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss tt"); ;

            ViewBag.Error = TempData[""];

            //DataLayer Pdal = new DataLayer();
            List<Product> objProducts =
             (from x in Pdal.Products
              where (x.productID.Equals(o.productID))//Finding the desired product in data base
              select x).ToList<Product>();

            if ((objProducts[0].initialAmount >= o.amount))//If there is enough in stock you can to buy
            {
                List<Order> objO = Pdal.Orders.ToList<Order>();
                //find the max orderID
                var maxOrderID = -1;
                foreach (Order order in objO)
                {
                    if (Int32.Parse(order.orderID) > maxOrderID)
                    {
                        maxOrderID = Int32.Parse(order.orderID);
                    }
                }
                o.orderID = (maxOrderID + 1).ToString();
                //o.productName = objProducts[0].productName;
                objProducts[0].initialAmount -= o.amount;
                Pdal.Orders.Add(o);//add order to data base

                try
                {
                    Pdal.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    ViewBag.Error = TempData["there was problem. please try again"];
                }
            }
            else ViewBag.Error = TempData["not enough in stock"];
            List<Order> objOrders = Pdal.Orders.ToList<Order>();
            objProducts = Pdal.Products.ToList<Product>();
            return Json(objProducts, JsonRequestBehavior.AllowGet);//return json with all the current products 
        }

        [HttpDelete]
        public JsonResult DeleteOrderByJson(string id)
        {
            DataLayer Pdal = new DataLayer();
            List<Order> objOrders =
               (from x in Pdal.Orders
                where (x.orderID.Equals(id.ToString()))
                select x).ToList<Order>();
            if (objOrders.Count == 1)
            {
                Pdal.Orders.Remove(objOrders[0]);
                Pdal.SaveChanges();//update data base
            }
            return GetOrderByJson();
        }

        [HttpPut]
        public JsonResult ChangeOrderAmount(string id, string orderAmount, string newAmount)
        {
            DataLayer Pdal = new DataLayer();
            List<Order> objOrders =
               (from x in Pdal.Orders
                where (x.orderID.Equals(id.ToString()))
                select x).ToList<Order>();
            if (objOrders == null) return null;
            var productID = objOrders.First().productID;
            //change the product amount left
            List<Product> productToEdit =
             (from x in Pdal.Products
              where (x.productID.Equals(productID))//Finding the desired product in data base
              select x).ToList<Product>();
            // current amount = DB amount + amount that were ordered(return the bought) - the new amount bought;
            productToEdit.First().initialAmount = productToEdit.First().initialAmount + Int32.Parse(orderAmount) - Int32.Parse(newAmount);
            //update the order amount
            objOrders.First().amount = Int32.Parse(newAmount);
            Pdal.SaveChanges();//update data base

            return GetOrderByJson();
        }
        [HttpPost]
        public JsonResult SearchOrder(string productID, string productName, string supplierID)
        {
            var orderedProducts = GetOrderedProducts();
            List<OrderProductViewModel> results = null;

            foreach (var orderProduct in orderedProducts)
            {
                results =
                  (from x in orderedProducts
                   where (x.order != null) && ((productID != "All" ? x.product.productID.Equals(productID) : true) && (productName != "All" ? x.product.productType.Equals(productName) : true)
                   && (supplierID != "" ? (x.order != null && x.order.supplierID.Equals(supplierID)) : true))
                   select x).ToList();
            }

            return Json(results, JsonRequestBehavior.AllowGet);
        }
    }


}
