﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace MVCTutorial.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ViewResult Index()
        {
            AuthController.RemoveCookies();
            Session["PageTitle"] = "Clothes Stock";
            return View("ShowHomePage");
        }

        public ViewResult ShowHomePage()
        {
            AuthController.RemoveCookies();
            Session["PageTitle"] = "Clothes Stock";
            return View();
        }
        
    }
}