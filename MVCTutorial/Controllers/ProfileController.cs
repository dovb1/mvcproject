﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTutorial.Models;
using MVCTutorial.Dal;
using MVCTutorial.ViewModel;
using System.Threading;
using System.Web.Security;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MVCTutorial.Controllers
{
    public class ProfileController : Controller
    {
        public ActionResult ShowProfilePage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null || HttpContext.Request.Cookies["Manager"] != null)
            {
                string data = null;
                if (HttpContext.Request.Cookies["Supplier"] != null)
                {
                    data = HttpContext.Request.Cookies.Get("Supplier").Value;
                }

                if (HttpContext.Request.Cookies["Manager"] != null)
                {
                    data = HttpContext.Request.Cookies.Get("Manager").Value;
                }
            


                User user = new User();
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                Session["PageTitle"] = "Profile";
                return View(user);
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        public ActionResult UpdateProfileDetails()
        {
            //To take data from from and update in DataAnnotationsModelMetadata base
            String userID = Request.Form["userID"];
            DataLayer Pdal = new DataLayer();
            List<User> userToEdit =
               (from x in Pdal.Users
                where (x.userID.Equals(userID))
                select x).ToList<User>();//find the product in data base

            userToEdit.First().name = Request.Form["name"];
            userToEdit.First().password = Request.Form["password"];
            Pdal.SaveChanges();//update data base
            userToEdit = Pdal.Users.ToList<User>();
            AuthController.RemoveCookies();
            return ShowProfilePage();
        }
    }
}
