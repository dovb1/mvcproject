﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTutorial.Models;
using MVCTutorial.Dal;
using MVCTutorial.ViewModel;
using System.Threading;
using System.Web.Security;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace MVCTutorial.Controllers
{
    //[Authorize] //Have access only to suppliers
    public class SupplierController : Controller
    {
        // GET: supplier
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowSupplierPage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null)
            {
                User user = new User();
                String data = HttpContext.Request.Cookies.Get("Supplier").Value;
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                Session["LogginInUser"] = user.name;
                Session["PageTitle"] = "Recommended";
                return View(user);//return view of supplier home page
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        public ActionResult ShowProfilePage()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null || HttpContext.Request.Cookies["Manager"] != null)
            {
                String data = HttpContext.Request.Cookies.Get("Supplier").Value;
                if (HttpContext.Request.Cookies["Manager"] != null)
                {
                    data = HttpContext.Request.Cookies.Get("Manager").Value;
                }


                User user = new User();
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                Session["LogginInUser"] = user.name;
                Session["PageTitle"] = "Profile";
                return View(user);
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        public ActionResult ShowSuppliers()
        {
            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                String data = HttpContext.Request.Cookies.Get("Manager").Value;
                Session["LogginInUser"] = (String)JObject.Parse(data)["name"];
                Session["PageTitle"] = "Suppliers";
                return View();
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        public JsonResult GetSuppliersByJson()
        {
            DataLayer Pdal = new DataLayer();
            int permission = 0;
            List<User> objSuppliers = (from user in Pdal.Users
                                       where user.permission.Equals(permission)
                                       select user).ToList<User>();

            return Json(objSuppliers, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteSupplierByJson(string id)
        {
            DataLayer Pdal = new DataLayer();
            List<User> objSupplier =
               (from x in Pdal.Users
                where (x.userID.Equals(id.ToString()))
                select x).ToList<User>();
            if (objSupplier.Count == 1)
            {
                Pdal.Users.Remove(objSupplier[0]);
                Pdal.SaveChanges();//update data base
            }
            return GetSuppliersByJson();
        }

        public ActionResult ShowMyReservations()
        {
            if (HttpContext.Request.Cookies["Supplier"] != null)
            {
                Session["PageTitle"] = "My Chart";
                return View();
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }
        public JsonResult GetMyReservations()
        {
            String data = HttpContext.Request.Cookies.Get("Supplier").Value;
            String currentUserID = (String)JObject.Parse(data)["userID"];
            DataLayer Pdal = new DataLayer();
            List<Order> objOrders = Pdal.Orders.ToList<Order>();
            List<Product> objProducts = Pdal.Products.ToList<Product>();

            var AllOrders = (from ord in objOrders
                                join prod in objProducts on ord.productID equals prod.productID
                                where currentUserID == ord.supplierID
                                select new OrderProductViewModel
                                {
                                    order = ord,
                                    product = prod

                                }).ToList();
            
            return Json(AllOrders, JsonRequestBehavior.AllowGet);
            

        }
    }
}
                   
                


            
    

