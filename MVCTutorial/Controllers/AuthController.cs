﻿using MVCTutorial.Dal;
using MVCTutorial.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MVCTutorial.ViewModel;
using Newtonsoft.Json;
using MVCTutorial.Controllers;

namespace MVCTutorial.Controllers
{
    public class AuthController : Controller
    {
        // GET: Login
        public ViewResult Index()
        {
            RemoveCookies();
            Session["PageTitle"] = "Clothes Stock";
            return View("Auth", new User());//return view of login with empty instance of manager
        }

        public ViewResult ShowSignInPage()
        {
            RemoveCookies();
            Session["PageTitle"] = "Clothes Stock";
            return View();
        }

        public ViewResult ShowSignUpPage()
        {
            Session["PageTitle"] = "Clothes Stock";
            return View();
        }

        public ActionResult SignIn(User user)//Action which come after clicking the Login
        {
            //ModelState.Remove("name");//????if name removed when submitted it will be not required///
            ModelState["name"].Errors.Clear();
            TempData["incorrect_details"] = "";
            if (ModelState.IsValid)//If the form has been validated,  user authentication is done
            {
                DataLayer Mdal = new DataLayer();
                List<User> objUsers =  //Looking manager with an ID and password are the same data entered
                       (from x in Mdal.Users
                        where ((x.userID.Equals(user.userID)) && (x.password.Equals(user.password)))
                        select x).ToList<User>();
                if (objUsers.Count == 1)//If found the user is sent to the manager page
                {
                    //user = objUsers[0];
                    //FormsAuthentication.SetAuthCookie("cookie", true);
                    if (objUsers[0].permission == 1)
                    {
                        //return Json(objUsers[0], JsonRequestBehavior.AllowGet);
                        CreateCookie("Manager", objUsers[0]);
                        return RedirectToAction("ShowManagerPage", "Manager");

                    }
                    else
                    {
                        //get the supplier name by join
                        List<User> suppliers = (from x in Mdal.Users
                                                where ((x.userID.Equals(user.userID)) && (x.password.Equals(user.password)))
                                                select x).ToList<User>();
                        //user = suppliers[0];
                        // return Json(suppliers[0], JsonRequestBehavior.AllowGet);
                        CreateCookie("Supplier", objUsers[0]);
                        return RedirectToAction("ShowSupplierPage", "Supplier");
                    }

                }

                else//If not found the user stays on the login page with the data it has entered
                {
                    TempData["incorrect_details"] = "User or password are wrong!";
                    return RedirectToAction("ShowSignInPage", "Auth");
                    //return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ModelState.Clear();
                //return Json(null, JsonRequestBehavior.AllowGet);
                return RedirectToAction("ShowSignInPage", "Auth");//If the form was not validated the user stays on the login page with the data it has entered

            }
        }

        public ActionResult SignUp(User user)//adding suplier
        {
            TempData["Message"] = ""; //same user error message
            DataLayer dl = new DataLayer();

            if (ModelState.IsValid)
            {
                List<User> exists =//check if the suplier already exists
                (from x in dl.Users
                 where (x.userID.Equals(user.userID.ToString()))
                 select x).ToList<User>();
                if (exists.Count == 0)//if not exists-suplier added
                {
                    dl.Users.Add(user);
                    dl.SaveChanges();//update data base
                    ViewBag.Message = "";
                    CreateCookie("Supplier", user);
                    return RedirectToAction("ShowSupplierPage", "Supplier");
                }
                else
                {
                    TempData["Message"] = "ID already exists..";
                    return RedirectToAction("ShowSignUpPage", "Auth");
                }
            }

            else
            {
                TempData["Message"] = "";
                return RedirectToAction("ShowSignUpPage", "Auth");
            }

            //else ViewBag.Message = "The Suplier Already Exists";
            //List<User> objSupliers = dl.Users.ToList<User>();
            //return Json(objSupliers, JsonRequestBehavior.AllowGet);//return json with the updated supliers list
        }

        public static void CreateCookie(String role, User user)
        {
            HttpCookie cookie = new HttpCookie(role);
            cookie.Value = JsonConvert.SerializeObject(user);
            cookie.Expires = DateTime.Now.AddHours(24);
            System.Web.HttpContext.Current.Response.SetCookie(cookie);
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void RemoveCookies()
        {
            if (System.Web.HttpContext.Current != null)
            {
                int cookieCount = System.Web.HttpContext.Current.Request.Cookies.Count;
                for (var i = 0; i < cookieCount; i++)
                {
                    var cookie = System.Web.HttpContext.Current.Request.Cookies[i];
                    if (cookie != null)
                    {
                        var expiredCookie = new HttpCookie(cookie.Name)
                        {
                            Expires = DateTime.Now.AddDays(-1),
                            Domain = cookie.Domain
                        };
                        System.Web.HttpContext.Current.Response.Cookies.Add(expiredCookie); // overwrite it
                    }
                }

                // clear cookies server side
                System.Web.HttpContext.Current.Request.Cookies.Clear();
            }
        }
    }


}
