﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTutorial.Models;
using MVCTutorial.Dal;
using MVCTutorial.ViewModel;
using System.Web.Security;
using System.Threading;
using System.Data;
using GenericParsing;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json.Linq;
using Accord.MachineLearning.Bayes;
using Accord.Statistics.Distributions.Univariate;
using Accord.Statistics.Distributions.Fitting;
using System.Threading.Tasks;
using System.Net.Http;
using System.Windows.Forms;

namespace MVCTutorial.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ViewResult Index()
        {
            return View();
        }
        public ActionResult ShowNewProductPage()//return view of add product page
        {
            /*
             
             ------------------------------!!!!!-----------------------------------
             
             //problem when not valid product has been added
            //dont need to show all the products thus the modelview is not needed    
             
             */

            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                DataLayer Pdal = new DataLayer();
                List<Product> objProducts = Pdal.Products.ToList<Product>();
                ProductViewModel pvm = new ProductViewModel();
                pvm.product = new Product();
                pvm.products = objProducts;
                Session["PageTitle"] = "New Product";
                return View(pvm);
            }
            else
            {
                return RedirectToAction("ShowHomePage", "Home");
            }


        }

        // web service!!
        public async Task<JsonResult> GetCurrentDollarJson(string currCoin)
        {
            HttpClient client = new HttpClient();
            var responseString = await client.GetStringAsync("https://api.exchangeratesapi.io/latest?symbols=" + currCoin);
            return Json(responseString, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowStock()//return view of show stock page
        {
            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                DataLayer Pdal = new DataLayer();
                List<Product> objProducts = Pdal.Products.ToList<Product>();
                ProductViewModel pvm = new ProductViewModel();
                pvm.product = new Product();
                pvm.products = objProducts;
                Session["PageTitle"] = "Our Products";
                return View(pvm);
            }
            else
            {
                return RedirectToAction("ShowHomePage", "Home");
            }
        }
        public JsonResult GetProductByJson()//return json with all the current products
        {
            DataLayer Pdal = new DataLayer();
            List<Product> objProducts = Pdal.Products.ToList<Product>();
            return Json(objProducts, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddProduct()//adding product
        {
            DataLayer Pdal = new DataLayer();
            Product p = new Product();
            List<Product> objO = Pdal.Products.ToList<Product>();
            //find the max orderID
            var maxProductID = -1;
            foreach (Product product in objO)
            {
                if (Int32.Parse(product.productID) > maxProductID)
                {
                    maxProductID = Int32.Parse(product.productID);
                }
            }
            p.productID = (maxProductID + 1).ToString();
            p.productType = Request.Form["productType"].ToString();
            p.price = Convert.ToInt32(Request.Form["product.price"].ToString());
            p.initialAmount = Convert.ToInt32(Request.Form["product.initialAmount"].ToString());
            p.color = Request.Form["productColor"].ToString();
            p.size = Convert.ToInt32(Request.Form["productSize"].ToString());
            p.productCompany = Request.Form["productCompany"].ToString();




            HttpPostedFileBase filePosted = Request.Files["inputFile"];

            if (filePosted != null && filePosted.ContentLength > 0)
            {
                string fileNameApplication = System.IO.Path.GetFileName(filePosted.FileName);
                string fileExtensionApplication = System.IO.Path.GetExtension(fileNameApplication);

                // generating a random guid for a new file at server for the uploaded file
                string newFile = Guid.NewGuid().ToString() + fileExtensionApplication;
                // getting a valid server path to save
                string filePath = System.IO.Path.Combine(Server.MapPath("~/Content/img/Products"), newFile);

                if (fileNameApplication != String.Empty)
                {
                    filePosted.SaveAs(filePath);
                }
            }




            DataLayer dl = new DataLayer();
            List<Product> exists =//check if the product already exists
                (from x in dl.Products
                 where (x.productID.Equals(p.productID.ToString()))
                 select x).ToList<Product>();
            if (exists.Count == 0)//if not exists-product added
            {
                if (ModelState.IsValid)
                {
                    dl.Products.Add(p);
                    dl.SaveChanges();//update data base
                    List<Product> objProducts = dl.Products.ToList<Product>();
                    return Json(objProducts, JsonRequestBehavior.AllowGet);//return json with the updated product list
                }
            }
            return null;
        }
        public JsonResult DeleteProduct()//delete product
        {
            String id = Request.Form["product.productID"];
            DataLayer Pdal = new DataLayer();
            List<Product> objProducts =
               (from x in Pdal.Products
                where (x.productID.Equals(id.ToString()))
                select x).ToList<Product>();//find the product in data base
            if (objProducts.Count == 1)//After finding the product-the product deleted
            {
                //Thread.Sleep(3000);
                //return View("Delete");
                Pdal.Products.Remove(objProducts[0]);
                string idToRemove = objProducts[0].productID;
                string pathToDelete=(AppDomain.CurrentDomain.BaseDirectory + "Content\\img\\Products\\"+idToRemove+".png");
                System.IO.File.Delete(pathToDelete);
                Pdal.SaveChanges();//update data base
                ViewBag.DeleteMessage = "Deletion Is Complete";
            }
            objProducts = Pdal.Products.ToList<Product>();
            return Json(objProducts, JsonRequestBehavior.AllowGet);//return json with the updated product list
        }
        public JsonResult Edit()//edit product
        {
            Product p = new Product();
            p.productID = Request.Form["product.productID"].ToString();
            p.productType = Request.Form["product.productType"].ToString();
            p.price = Convert.ToInt32(Request.Form["product.price"].ToString());
            p.initialAmount = Convert.ToInt32(Request.Form["product.initialAmount"].ToString());
            p.color = Request.Form["product.color"].ToString();
            p.size = Convert.ToInt32(Request.Form["product.size"].ToString());
            p.productCompany = Request.Form["product.productCompany"].ToString();

            DataLayer Pdal = new DataLayer();
            List<Product> objProducts =
               (from x in Pdal.Products
                where (x.productID.Equals(p.productID.ToString()))
                select x).ToList<Product>();//find the product in data base
            if (objProducts.Count == 1)//After finding the product-the new details updated in data base
            {
                objProducts[0].productType = p.productType;
                objProducts[0].price = p.price;
                objProducts[0].initialAmount = p.initialAmount;
                objProducts[0].size = p.size;
                objProducts[0].productCompany = p.productCompany;
                objProducts[0].color = p.color;

                Pdal.SaveChanges();//update data base
            }

            objProducts = Pdal.Products.ToList<Product>();
            return Json(objProducts, JsonRequestBehavior.AllowGet);//return json with the updated product list

        }


        public List<int> ReccomnededProductsForUser()
        {
            var reccomendedProducts = new List<int>();
            // Get the user ID
            String data = HttpContext.Request.Cookies.Get("Supplier").Value;
            string userID = (string)JObject.Parse(data)["userID"];

            // Load the train data to dataTable
            var adapter = new GenericParsing.GenericParserAdapter(Server.MapPath("~/App_Data/OrdersHistory.csv"));
            adapter.FirstRowHasHeader = true; // first row represents title 
            DataTable dt = adapter.GetDataTable();
  
            // Seperate the current user row from the orders history
            DataTable curUserOrders = dt.Clone();
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt.Rows[i];
                
                if (dr[0].ToString() == userID)
                {
                    curUserOrders.ImportRow(dr);
                    dr.Delete();
                }
            }
            dt.AcceptChanges();

            // Remove the userId columns (no longer needed)
            dt.Columns.Remove("userId");
            curUserOrders.Columns.RemoveAt(0);

            // Check if the user exist in the csv - if not return emty list
            if(curUserOrders.Rows.Count == 0)
                return reccomendedProducts;

            // Loop through product and rate them
            int productIndex = 0;
            foreach (DataColumn col in dt.Columns)
            {
                // Score the product only if the current user didnt buy it
                if (curUserOrders.Rows[0][productIndex].ToString() == "0")
                {

                    // separate the current product column(y) from the train data(x)
                    DataTable tempDt = dt.Copy();
                    int[] curProductOrders = Array.ConvertAll(tempDt.AsEnumerable().Select(r => r.Field<string>(col.ColumnName)).ToArray(), s => int.Parse(s));
                    tempDt.Columns.Remove(col.ColumnName);
                    var z = tempDt.Rows.Cast<DataRow>().Select(r => r.ItemArray).ToArray();
                    double[][] otherProductsOrders = new double[z.Length][];
                    for (int i = 0; i < z.Length; i++)
                        otherProductsOrders[i] = Array.ConvertAll(z[i].Cast<string>().ToArray(), s => double.Parse(s));

                    // Build the model
                    var teacher = new NaiveBayesLearning<NormalDistribution>();
                    teacher.Options.InnerOption = new NormalOptions
                    {
                        Regularization = 1e-5 // to avoid zero variances
                    };
                    NaiveBayes<NormalDistribution> bayes = teacher.Learn(otherProductsOrders, curProductOrders);

                    // Generate the current user vector without the current product
                    using (DataTable tempcurProducts = curUserOrders.Copy())
                    {
                        tempcurProducts.Columns.Remove(col.ColumnName);
                        double[] usersProductsWithoutCurr = Array.ConvertAll(tempcurProducts.Rows.Cast<DataRow>()
                                                                    .Select(r => r.ItemArray)
                                                                    .ToArray()[0]
                                                                    .Cast<string>()
                                                                    .ToArray(), x => double.Parse(x));

                        // The prediction - if answer equals 1 then we reccomend this product
                        int answer = bayes.Decide(usersProductsWithoutCurr);

                        if(answer==1)
                        {
                            reccomendedProducts.Add(int.Parse(col.ColumnName));
                        }
                    }
                }

                productIndex++;
            }

            return reccomendedProducts;
        }



        public JsonResult GetReccomnededProducts()//return json with all the reccomned products
        {
            List<int> productIds = ReccomnededProductsForUser();
            DataLayer Pdal = new DataLayer();
            List<Product> objProducts = Pdal.Products.Where(p=> productIds.Any(id=> id.ToString() ==p.productID)).ToList<Product>();
            return Json(objProducts, JsonRequestBehavior.AllowGet);
        }


        public string GetIdOfNewProduct()
        {
            int maxID = -1;
            DataLayer Pdal = new DataLayer();
            List<Product> objProducts =
               (from x in Pdal.Products
                select x).ToList();
            foreach (Product product in objProducts)
            {
                if (Int32.Parse(product.productID) > maxID)
                {
                    maxID = Int32.Parse(product.productID);
                }
            }
            return maxID.ToString();
        }
        [HttpPost]
        public ActionResult SaveImages(HttpPostedFileBase UploadedImage)
        {
            if (UploadedImage?.ContentLength > 0)
            {
                String ImageFileName = Path.GetFileName(UploadedImage.FileName);
                string FolderPath = Path.Combine(Server.MapPath("~/Content/img/Products"), ImageFileName);
                UploadedImage.SaveAs(FolderPath);
                int lastIndexBeforeName = FolderPath.LastIndexOf("\\");
                //int lastIndexOfDot = FolderPath.LastIndexOf(".");
                //string extention = FolderPath.Substring(lastIndexOfDot);
                string newID = GetIdOfNewProduct();
                string newName = FolderPath.Substring(0, lastIndexBeforeName + 1) + newID + ".png";
                System.IO.File.Copy(FolderPath, newName, true);
                System.IO.File.Delete(FolderPath);
            }
            
            return RedirectToAction("ShowStock", "Product");
        }

        public JsonResult SearchProduct(string productType, string productCompany,string productSize, string productPrice, string productColor)
        {
            int size = 0, minPrice = 1, maxPrice = 0;
            if (productPrice != "All")
            {
                string[] priceRange = productPrice.Split('-');

                minPrice = Convert.ToInt32(priceRange[0]);
                maxPrice = Convert.ToInt32(priceRange[1]);
            }
            if (productSize != "All")
            {
                size = Convert.ToInt32(productSize);
            }
            DataLayer Pdal = new DataLayer();
            List<Product> objFilterdProducts =
                (from x in Pdal.Products
                 where ((productType != "All" ? x.productType.Equals(productType) : true) && (productCompany != "All" ? x.productCompany.Equals(productCompany) : true)
                 && (productSize != "All" ? x.size.Equals(size) : true) && (productPrice != "All" ? (x.price >= minPrice && x.price <= maxPrice) : true) && (productColor != "All" ? x.color.Equals(productColor) : true))
                 select x).ToList<Product>();
            return Json(objFilterdProducts, JsonRequestBehavior.AllowGet);


        } 
    }


}