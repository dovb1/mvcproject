﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCTutorial.Models;
using MVCTutorial.Dal;
using MVCTutorial.ViewModel;
using System.Web.Security;
using System.Threading;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;


namespace MVCTutorial.Controllers
{
    //[Authorize]
    //Have access only to managers
    public class ManagerController : Controller
    {

        // GET: Manager
        public ViewResult Index()
        {
            return View();
        }

        public ActionResult ShowManagerPage()
        {
            if (HttpContext.Request.Cookies["Manager"] != null)
            {
                String data = HttpContext.Request.Cookies.Get("Manager").Value;
                User user = new User();
                user.userID = (String)JObject.Parse(data)["userID"];
                user.name = (String)JObject.Parse(data)["name"];
                user.permission = (int)JObject.Parse(data)["permission"];
                user.password = (String)JObject.Parse(data)["password"];
                getMostSellingProducts();
                getSupplierBuying();
                Session["LogginInUser"] = user.name;
                Session["PageTitle"] = "Dashboard";
                return View(user);//return view of manager home page
            }
            else
            {
                return RedirectToAction("ShowSignInPage", "Auth");
            }
        }

        public void getMostSellingProducts()
        {
            DataLayer Pdal = new DataLayer();
            List<Order> objOrders = Pdal.Orders.ToList<Order>();
            List<Product> objProducts = Pdal.Products.ToList<Product>();

            var sells = objOrders.GroupBy(ord => ord.productID)
                                 .Select(ord => new { amount = ord.Sum(b => b.amount), productID = ord.Key }).ToList();

            var mostSellingProducts = (from product in objProducts
                                 join sell in sells
                                 on product.productID equals sell.productID
                                 into result
                                 from newSell in result.DefaultIfEmpty()
                                 select new SellProductViewModel { productType = product.productType, productCompany = product.productCompany, price = newSell != null ? newSell.amount * product.price : 0 }).Take(10).ToArray();

            writeToFile("MostSellingProducts.csv", mostSellingProducts);

        }

        public void getSupplierBuying()
        {
            DataLayer Pdal = new DataLayer();
            List<Order> objOrders = Pdal.Orders.ToList<Order>();
            List<Product> objProducts = Pdal.Products.ToList<Product>();
            
            var sells = objOrders.GroupBy(ord => new { ord.supplierID, ord.productID })
                                 .Select(ord => new { amount = ord.Sum(b => b.amount), suppAndProd = ord.Key }).ToList();

            var suppliersBuying = (from product in objProducts
                                  join sell in sells
                                  on product.productID equals sell.suppAndProd.productID
                                  into result
                                  from newSell in result.DefaultIfEmpty()
                                  select new SupplierProductViewModel { productType = product.productType, supplierID = newSell != null ? newSell.suppAndProd.supplierID : "no", productCompany = product.productCompany, price = newSell != null ? newSell.amount * product.price : 0 }).Take(10).ToArray();

            writeToFile("SupplierBuying.csv", null, suppliersBuying);
        }


        public void writeToFile(String fileName, SellProductViewModel[] SellProductArray = null, SupplierProductViewModel[] SupplierProductArray = null)
        {
            using (System.IO.StreamWriter file =
             new System.IO.StreamWriter(System.AppDomain.CurrentDomain.BaseDirectory + fileName))
            {
                if (SellProductArray != null)
                {
                    file.WriteLine("productType,productCompany,price");
                    foreach (var product in SellProductArray)
                    {
                        file.WriteLine(product.productType + "," + product.productCompany + "," + product.price);
                    }
                }
                else
                {
                    file.WriteLine("productType,productCompany,supplierID,price");
                    foreach (var product in SupplierProductArray)
                    {
                        file.WriteLine(product.productType + "," + product.productCompany + "," + product.supplierID + "," + product.price);
                    }
                }
            }
        }
        
    }
}