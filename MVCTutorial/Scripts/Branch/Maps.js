﻿function getData() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: "/Branch/getLocations",
        success: function (data) { loadMapScenario(JSON.parse(data)); },
        error: function (data) { console.log(data); }
    });
}


function loadMapScenario(locations) {

    var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {});

    locations.map((location, i) => {
        var locationPosition = map.tryPixelToLocation(new Microsoft.Maps.Point(JSON.parse(location.coordinates).x, JSON.parse(location.coordinates).y));
        var pushpin = new Microsoft.Maps.Pushpin(locationPosition, null);
        var infobox = new Microsoft.Maps.Infobox(locationPosition, { title: location.title, description: location.description, visible: false });
        infobox.setMap(map);
        Microsoft.Maps.Events.addHandler(pushpin, 'click', function () {
            infobox.setOptions({ visible: true });
        });
        map.entities.push(pushpin);
    })
}