﻿$("#search-input").focus()
$("#search-input").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("tbody tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
    //if no matches found show the div
    hiddenElements = $('tbody tr:hidden');
    if (hiddenElements.length == $('#supplierTable tr').length) {
        $("thead").hide();
        $(".no-suppliers-div").removeClass('hidden');
    }
    else {
        $("thead").show();
        $('.no-suppliers-div').addClass('hidden');
    }
       
});

$(".reset-icon").on("click", () => {
    $("tbody tr").show();
    $("thead").show();
    $('.no-suppliers-div').addClass('hidden');
    $("#search-input").val("");
    $("#search-input").focus();
})

function ScrollUp() {
    document.body.scrollIntoView({ behavior: 'smooth', block: 'start' });
}

