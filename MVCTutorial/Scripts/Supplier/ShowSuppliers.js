﻿$.get("GetSuppliersByJson", null, DisplaySuppliers);

function DisplaySuppliers(suppliers) {
    var Ptbl = $("#supplierTable");
    $("#supplierTable").find("tr").remove();

    for (i = 0; i < suppliers.length; i++) {
        if (suppliers[i].userID != null) {
            var newRow = "<tr id=" + suppliers[i].userID + ">" +
                "<td class=supid>" + suppliers[i].userID + "</td>" +
                "<td class=supprodid>" + suppliers[i].name + "</td>" +
                "<td class=supname>" + suppliers[i].password + "</td>" +
                "<td class=delete-supplier>" + "<i class='fas fa-trash fa-lg delete-btn' onclick='ConfirmDelete(" + suppliers[i].userID + ")'></i>" + "</td>" +
                "</tr>";
            Ptbl.append(newRow);
        }
    }

}
function ConfirmDelete(id) {
    Swal.fire({
        title: "Are you sure?",
        text: "The supplier will be deleted permenantly!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonColor: '#3085d6',
    }).then((result) => {
        if (result.value)
            DeleteSupplier(id)
    })
}

function DeleteSupplier(id) {
    console.log(id)
    var SupplierToDelete = JSON.stringify({ "ID": id });

    $.ajax({
        type: "DELETE",
        url: '/Supplier/DeleteSupplierByJson',
        data: SupplierToDelete,
        contentType: 'application/json; charset=utf-8',
        success: DisplaySuppliers
    }).then(() => { DisplayToast("success", "Supplier deleted successfully!", "green") })

}

function DisplayToast(status, message, backgroundColor) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        background: backgroundColor,
        timer: 3000
    })
    Toast.fire({
        type: status,
        title: message
    })
}

