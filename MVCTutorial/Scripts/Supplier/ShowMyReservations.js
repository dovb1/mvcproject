﻿$.get("/Supplier/GetMyReservations", null, DisplayAllProducts);

function DisplayAllProducts(products) {
    var Ptbl = $("#productTable");
    $("#productTable").find("tr:gt(0)").remove();
    for (i = 0; i < products.length; i++) {
        if (products[i].initialAmount != 0) {
            var newRow = "<tr id=" + products[i].product.productID + ">" +
                "<td class=prodID>" + products[i].product.productID + "</td>" +
                "<td class=prodtype>" + products[i].product.productType + "</td>" +
                "<td class=prodcompany>" + products[i].product.productCompany + "</td>" +
                "<td class=prodsize>" + products[i].product.size + "</td>" +
                "<td class=prodprice>" + products[i].product.price + "</td>" +
                "<td class=prodcolor>" + products[i].product.color + "</td>" +
                "<td class=prodamount>" + products[i].order.amount + "</td>" +
                "<td class=ordDate>" + products[i].order.orderDate + "</td>" +
                "</tr>";
            Ptbl.append(newRow);
        }
    }
    $("#status").text("");
}