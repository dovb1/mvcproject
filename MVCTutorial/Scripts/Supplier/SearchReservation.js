﻿$("#search-input").focus()
$("#search-input").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("tbody tr").filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
    //if no matches found show the div
    hiddenElements = $('tbody tr:hidden');
    if (hiddenElements.length == $('#productTable tr').length) {
        $("thead").hide();
        $(".no-reservations-div").removeClass('hidden');
    }
    else {
        $("thead").show();
        $('.no-reservations-div').addClass('hidden');
    }

});

$(".reset-icon").on("click", () => {
    $("tbody tr").show();
    $("thead").show();
    $('.no-reservations-div').addClass('hidden');
    $('.no-reservation-div').addClass('hidden');
    $("#search-input").val("");
    $("#search-input").focus();
})

function ScrollUp() {
    $('html,body').animate({ scrollTop: 0 }, 1000);
}
