﻿function CreateSelectOptions(optionsArr,selectorID) {
    var select = $("#" + selectorID)
    var option;
    optionsArr.forEach((value) => {
        option = document.createElement("option")
        option.text = value;
        select.append(option)
    })
}