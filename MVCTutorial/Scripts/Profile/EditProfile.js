﻿function UpdateProfile() {
    $("#editForm").submit(function (e) {
        console.log("Form sent!")
        e.preventDefault();
        if ($("#editForm").valid()) {
            var frmData = $("#editForm").serialize(); // get the text that the user entered from the form
            $.post("UpdateProfileDetails", frmData, EditSuccess);
        }

    });
}

function EditSuccess() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        background: 'green',
        timer: 3000
    })
    Toast.fire({
        type: 'success',
        title: "Details have been updated successfully!"
    })

}


UpdateProfile()

