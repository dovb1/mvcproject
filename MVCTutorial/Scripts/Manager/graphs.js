﻿$.get("GetCurrentDollarJson", null, GetCurrency);

function GetCurrency(currentDollar) {
    let data = JSON.parse(currentDollar)
    $("#currentDollar").text("Base: " + data.base + ", Rates: USD: " + data.rates.USD);
}

var margin = { top: 100, right: 30, bottom: 100, left: 75 },
    width = 600 - margin.right - margin.left,
    height = 500 - margin.top - margin.bottom;


var svg = d3.select("#svg1")
    .append("svg")
    .attr({
        "width": width + margin.right + margin.left,
        "height": height + margin.top + margin.bottom
    })
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.right + ")");


// defining  x and y scales
var xScale = d3.scale.ordinal()
    .rangeRoundBands([0, width], 0.2, 0.2);

var yScale = d3.scale.linear()
    .range([height, 0]);

// defining x axis and y axis
var xAxis = d3.svg.axis()
    .scale(xScale)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(yScale)
    .orient("left");

d3.csv("../MostSellingProducts.csv", function (error, data) {
    if (error) console.log("Error: data could  not be loaded!");
    data.forEach(function (d) {
        d.productCompany = d.productCompany + " " + d.productType;
        d.price = d.price;
    });

    // Specify the domains of the x and y scales
    xScale.domain(data.map(function (d) { return d.productCompany; }));
    yScale.domain([0, d3.max(data, function (d) { return d.price; })]);

    svg.selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
        .attr("height", 0)
        .attr("y", height)
        .transition().duration(3000)
        .delay(function (d, i) { return i * 200; })

        .attr({
            "x": function (d) { return xScale(d.productCompany); },
            "y": function (d) { return yScale(d.price); },
            "width": xScale.rangeBand(),
            "height": function (d) { return height - yScale(d.price); }
        })
        .style("fill", function (d, i) { return 'rgb(20, 20, ' + ((i * 30) + 100) + ')' });

   
    svg.selectAll('text')
        .data(data)
        .enter()
        .append('text')
        .text(function (d) {
            return d.price;
        })
        .attr({
            "x": function (d) { return xScale(d.productCompany) + xScale.rangeBand() / 2; },
            "y": function (d) { return yScale(d.price) + 12; },
            "font-family": 'sans-serif',
            "font-size": '13px',
            "font-weight": 'bold',
            "fill": 'white',
            "text-anchor": 'middle'
        });

     svg.append("text")
        .attr("x", (width / 2))
        .attr("y", 0 - (margin.top / 3))
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("text-decoration", "underline");


    // Drawing x axis and positioning the label
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .attr("dx", "-.8em")
        .attr("dy", ".25em")
        .attr("transform", "rotate(-60)")
        .style("text-anchor", "end")
        .attr("font-size", "10px");


    // Drawing  y Axis and positioning the label
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", -height / 2)
        .attr("dy", "-4em")
        .style("text-anchor", "middle")
        .text("Amount Dispensed");
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var margin2 = { top: 100, right: 60, bottom: 150, left: 75 },
    width2 = 660 - margin2.right - margin2.left,
    height2 = 500 - margin2.top - margin2.bottom;


var svg2 = d3.select("#svg2")
    .append("svg")
    .attr({
        "width": width2 + margin2.right + margin2.left,
        "height": height2 + margin2.top + margin2.bottom
    })
    .append("g")
    .attr("transform", "translate(" + margin2.left + "," + margin2.right + ")");


// defining  x and y scales
var xScale2 = d3.scale.ordinal()
    .rangeRoundBands([0, width2], 0.2, 0.2);

var yScale2 = d3.scale.linear()
    .range([height2, 0]);

// defining x axis and y axis
var xAxis2 = d3.svg.axis()
    .scale(xScale2)
    .orient("bottom");

var yAxis2 = d3.svg.axis()
    .scale(yScale2)
    .orient("left");

d3.csv("../SupplierBuying.csv", function (error, data) {
    if (error) console.log("Error: data could  not be loaded!");
    data.forEach(function (d) {
        d.productCompany = d.supplierID + " " + d.productCompany + " " + d.productType;
        d.price = d.price;
    });

    // Specify the domains of the x and y scales
    xScale2.domain(data.map(function (d) { return d.productCompany; }));
    yScale2.domain([0, d3.max(data, function (d) { return d.price; })]);

    svg2.selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
        .attr("height", 0)
        .attr("y", height2)
        .transition().duration(3000)
        .delay(function (d, i) { return i * 200; })

        .attr({
            "x": function (d) { return xScale2(d.productCompany); },
            "y": function (d) { return yScale2(d.price); },
            "width": xScale2.rangeBand(),
            "height": function (d) { return height2 - yScale2(d.price); }
        })
        .style("fill", function (d, i) { return 'rgb(20, 20, ' + ((i * 30) + 100) + ')' });

    
    svg2.selectAll('text')
        .data(data)
        .enter()
        .append('text')
        .text(function (d) {
            return d.price;
        })
        .attr({
            "x": function (d) { return xScale2(d.productCompany) + xScale2.rangeBand() / 2;},
            "y": function (d) { return yScale2(d.price) + 12; },
            "font-family": 'sans-serif',
            "font-size": '13px',
            "font-weight": 'bold',
            "fill": 'white',
            "text-anchor": 'middle'
        });

    svg2.append("text")
        .attr("x", (width2 / 2))
        .attr("y", 0 - (margin2.top / 3))
        .attr("text-anchor", "middle")
        .style("font-size", "16px")
        .style("text-decoration", "underline");


    // Drawing x axis and positioning the label
    svg2.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height2 + ")")
        .call(xAxis2)
        .selectAll("text")
        .attr("dx", "-.8em")
        .attr("dy", ".25em")
        .attr("transform", "rotate(-60)")
        .style("text-anchor", "end")
        .attr("font-size", "10px");


    // Drawing  y Axis and positioning the label
    svg2.append("g")
        .attr("class", "y axis")
        .call(yAxis2)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", -height2 / 2)
        .attr("dy", "-4em")
        .style("text-anchor", "middle")
        .text("amount");
});
