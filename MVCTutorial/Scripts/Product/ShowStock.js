﻿$("#status").text("Loading.........");
$.get("GetProductByJson", null, BindData);



function BindData(products) {
    var Ptbl = $("#productsView");
    var ptblTemp = $("#producttable")
    var size = 0;
    var productsHtml = "";
    for (i = 0; i < products.length; i++) {
        var newrow = "<tr id=" + products[i].productID + ">" +
            "<td class=prodid>" + products[i].productID + "</td>" +
            "<td class=prodtype>" + products[i].productType + "</td>" +
            "<td class=prodcompany>" + products[i].productCompany + "</td>" +
            "<td class=prodsize>" + products[i].size + "</td>" +
            "<td class=prodprice>" + products[i].price + "</td>" +
            "<td class=color>" + products[i].color + "</td>" +
            "<td class=prodamount>" + products[i].initialAmount + "</td>" +
            "</tr>";
        ptblTemp.append(newrow);

        productsHtml = productsHtml +
            "<div class='product-grid6'>" +
            "<div class='product-image6'>" +
            "<a href='#'>" +
            "<img height='100px' class='pic-1' src='/Content/img/Products/" + products[i].productID + ".png'>" +
            "</a>" +
            "</div>" +
            "<div class='product-content'>" +
            "<h3 class='title'><a href='#'>" + products[i].productCompany + " " + products[i].productType + "</a></h3>" +
            "<span class='size'>size:" + products[i].size + "       " + "Amount: " + products[i].initialAmount + "</span>" +
            "<div class='price'>" + products[i].price + "€" +
            "</div>" +
            "</div>" +
            "<ul class='social'>" +
            "<li><a onclick=Delete(" + products[i].productID + ") data-tip='Delete product'>" +
            "<img height='37px' src='/Content/img/delete.png'>" +
            "</a ></li > " +
            "<li><a data-toggle='modal' data-target='#myModal' onclick=Edit(" + products[i].productID + ") data-tip='Edit product'>" +
            "<img height='37px' src='/Content/img/edit.png'>" +
            "</i></a></li>" +
            "</ul>" +
            "</div>";

    }
    Ptbl.html(productsHtml);

    $("#status").text("");
}

function Edit(id) {

    var data = $("#" + id);
    $("#product_productType").val(data.find(".prodtype").text());
    $("#product_price").val(data.find(".prodprice").text());
    $("#productAmount").val(data.find(".prodamount").text());
    $("#product_productID").val(data.find(".prodid").text());
    $("#product_size").val(data.find(".prodSize").text());
    $("#product_productCompany").val(data.find(".prodCompany").text());
    $("#product_color").val(data.find(".color").text());
}

function Delete(id) {
    $("#status").text("Delete product........");
    var data = $("#" + id);
    $("#product_productID").val(data.find(".prodid").text());
    var frmData = $("#frmEditProd").serialize();
    $.post("DeleteProduct", frmData, BindData);

}

$("#frmEditProd").submit(function (e) {
    e.preventDefault();

    $("#status").text("Saving product........");
    var frmData = $("#frmEditProd").serialize();
    if ($("#frmEditProd").valid()) {
        $.post("Edit", frmData, editSuccess);
        $.get("GetProductByJson", null, BindData);
        $('#myModal').modal('hide');
        $('.modal-backdrop').remove();

    }


});

function editSuccess(d) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        background: 'green',
        timer: 3000
    })
    Toast.fire({
        type: 'success',
        title: "The product edited successfully!"
    })

    $.get("GetProductByJson", null, BindData);
}

function AddedProduct(products) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        background: 'green',
        timer: 3000
    })
    BindData(products);


    // Check if publish to facebook
    if ($('#isPublishOnFecebook').is(':checked') == true) {
        var message = 'New product added! product details: %0A product id: ' + $("#productId").val() + '%0A' +
            "Type:" + $("#productType").val() + '%0A' +
            "Company:" + $("#productCompany").val() + '%0A' +
            "size:" + $("#size").val() + '%0A' +
            "price:" + $("#price").val() + '%0A' +
            "color:" + $("#color").val() + '%0A';

        $.ajax({
            type: "POST",
            url: 'https://graph.facebook.com/101478944606926/feed?message=' + message + '!&access_token=EAALXDAZAn98EBACJRjueZAZCbP9gIWPMdhqdD4RbQT1vrkkUhq9TAijVZCZCQgePpsX7mHrMo768SbCit1txxIEah8rRV7jHLsu1osX2oqqskshZCJP8hxmNMiUzj3JFoeZA7Rr0NTvv7E7JjCnoR7M9J4Y34ZBrPc12UDUkHgXuuwZDZD',
            contentType: 'application/json; charset=utf-8',
        })
        $('#uploadImage').removeClass("hidden")
        $("#image-message").removeClass("hidden")
    }
}
var productType;
var userTypeOption;
var productCompany;
var userCompanyOption;
var productSize;
var userSizeOption;
var productColor;
var userColorOption;

function GetSelectedFields() {
    productType = document.getElementById("productType");
    userTypeOption = productType.options[productType.selectedIndex].value;
    productCompany = document.getElementById("productCompany");
    userCompanyOption = productCompany.options[productCompany.selectedIndex].value;
    productSize = document.getElementById("productSize");
    userSizeOption = productSize.options[productSize.selectedIndex].value;
    productColor = document.getElementById("productColor");
    userColorOption = productColor.options[productColor.selectedIndex].value;
}

$("#frmAddProd").submit(function (e) {
    GetSelectedFields();
    e.preventDefault();
    if ($("#frmAddProd").valid()) {
        if (userTypeOption != "Type" && userCompanyOption != "Company" && userSizeOption != "Size" && userColorOption != "Color") {
            $("#status").text("Saving product........");
            var frmData = $("#frmAddProd").serialize(); // get the text that the user entered from the form
            $.post("AddProduct", frmData, AddedProduct);
        }
        else {
            $("#invalid-select").removeClass("hidden")
        }

    }
});

$("select").on("change", () => {
    GetSelectedFields();
    if (userTypeOption != "Type" && userCompanyOption != "Company" && userSizeOption != "Size" && userColorOption != "Color") {
        $("#invalid-select").addClass("hidden")
    }
})



function ResetForm() {
    $("#product_initialAmount").val("");
    $("#product_price").val("");
    $("#product_productID").val("");
    $("#product_productType").val("");
    $("#product_size").val("");
    $("#product_color").val("");
    $("#product_productCompany").val("");

}

function ScrollUp() {
    jQuery("html,body").animate({ scrollTop: 20 }, 1000);
}