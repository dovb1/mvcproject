﻿$("#status").text("Loading.........");
$.get("GetProductByJson", null, DisplayProducts);

var addedNewProduct = true;

function DisplayProducts(products) {
    addedNewProduct = true;
    if (products != '') {
        $("#tblProd").find("tr:gt(0)").remove();//avoid duplication of table
        var tbl = $("#tblProd");
        for (k = 0; k < products.length; k++) {
            var newRow = "<tr>" +
                "<td>" + products[k].productID + "</td>" +
                "<td>" + products[k].productType + "</td>" +
                "<td>" + products[k].productCompany + "</td>" +
                "<td>" + products[k].size + "</td>" +
                "<td>" + products[k].price + "</td>" +
                "<td>" + products[k].color + "</td>" +
                "<td>" + products[k].initialAmount + "</td>" +
                "</tr>"
            tbl.append(newRow);
        }
        $("#status").text("");
        ResetForm();
        $("#product_productName").focus();

    } else {
        addedNewProduct = false;
        $("#status").text("Product already exists in the stock!");
    }

}

function AddedProduct(products) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    })
    DisplayProducts(products);
    if (addedNewProduct) {
  

        // Check if publish to facebook
        if ($('#isPublishOnFecebook').is(':checked') == true) {
            var message = 'New product added! product details: %0A product id: ' + $("#productId").val() + '%0A' +
                "Type:" + $("#productType").val() + '%0A' +
                "Company:" + $("#productCompany").val() + '%0A' +
                "size:" + $("#size").val() + '%0A' +
                "price:" + $("#price").val() + '%0A' +
                "color:" + $("#color").val() + '%0A';

            $.ajax({
                type: "POST",
                url: 'https://graph.facebook.com/101478944606926/feed?message=' + message +'!&access_token=EAALXDAZAn98EBACJRjueZAZCbP9gIWPMdhqdD4RbQT1vrkkUhq9TAijVZCZCQgePpsX7mHrMo768SbCit1txxIEah8rRV7jHLsu1osX2oqqskshZCJP8hxmNMiUzj3JFoeZA7Rr0NTvv7E7JjCnoR7M9J4Y34ZBrPc12UDUkHgXuuwZDZD',
                contentType: 'application/json; charset=utf-8',
            }).then(() => { DisplayToast("success", "Product published on facebook", "green") })
        }

        Toast.fire({
            type: 'success',
            title: 'Product Added successfully!'
        })
    }

    else {
        Toast.fire({
            type: 'warning',
            title: 'Product already exists!'
        })
    }
}
//fileField = document.getElementById("inputFile");
//fileField.addEventListener("change", function () {
//    $.ajax({
//        type: "POST",
//        data: JSON.stringify({ fileName: fileField.value }),
//        url: '/Product/CopyFile',
//        contentType: 'application/json; charset=utf-8',
//    })
//})

$("#frmAddProd").submit(function (e) {
    var productType = document.getElementById("productType");
    var userTypeOption = productType.options[productType.selectedIndex].value;
    var productCompany = document.getElementById("productCompany");
    var userCompanyOption = productCompany.options[productCompany.selectedIndex].value;
    var productSize = document.getElementById("productSize");
    var userSizeOption = productSize.options[productSize.selectedIndex].value;
    var productColor = document.getElementById("productColor");
    var userColorOption = productColor.options[productColor.selectedIndex].value;
    e.preventDefault();
    if ($("#frmAddProd").valid()) {
        if (userTypeOption != "Type" && userCompanyOption != "Company" && userSizeOption != "Size" && userColorOption != "Color") {
            $("#status").text("Saving product........");
            var frmData = $("#frmAddProd").serialize(); // get the text that the user entered from the form
            $.post("AddProduct", frmData, AddedProduct);
        }
        else {
            alert("Make sure all fields selected ! ")
        }
        
    }

});



function ResetForm() {
    $("#product_initialAmount").val("");
    $("#product_price").val("");
    $("#product_productID").val("");
    $("#product_productType").val("");
    $("#product_size").val("");
    $("#product_color").val("");
    $("#product_productCompany").val("");

}