﻿var sizes = [30, 32, 34, 36, 38, 40, 42, 44, 46, 48]
var productTypes = ["shirt", "polo", "shorts", "pants", "sweater", "jacket", "dress", "skirts", "top", "shoes", "jeans"];
var productCompanies = ["Adidas", "Nike", "C&A", "Calvin Klein", "Ralph Lauren", "Tommy Hilfiger", "Under Armour", "The North Face", "Guess", "Abercrombie & Fitch", "Forever 21"]
var productColors = ["Blue ", "Green ", "Brown", "Purple", "Yellow", "White", "Black", "Gray", "Lime"]

CreateSelectOptions(sizes, "productSize");
CreateSelectOptions(productTypes, "productType");
CreateSelectOptions(productCompanies, "productCompany");
CreateSelectOptions(productColors, "productColor");