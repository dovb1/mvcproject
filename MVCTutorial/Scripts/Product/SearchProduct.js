﻿var selectType = "All";
var selectCompany = "All";
var selectSize = "All";
var selectPrice = "All";
var selectColor = "All";

//var sel = $('#currency').val()


//sel.addEventListener("change", GetCurrentCoin);


$('#FilterSearchForm').submit((e) => {
    e.preventDefault();
    //var frmData = $("#FilterSearchForm").serialize()
    //$.post("/Product/SearchProduct", frmData, DisplayAllProducts)
    $.ajax({
        type: "POST",
        url: '/Product/SearchProduct',
        data: JSON.stringify({
            productType: selectType,
            productCompany: selectCompany,
            productSize: selectSize,
            productPrice: selectPrice,
            productColor: selectColor,
        }),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            GetCurrentCoin1($('#currency').val(), data)
            
        } 
    })
});

function GetCurrentCoin1(coin, data) {

    $.post("/Order/GetCurrentDollarJson", { "currCoin": coin }, function (coin1) { GetCurrency1(coin1, data) });


}

function GetCurrency1(currentCoin, data) {
    let data1 = JSON.parse(currentCoin)
    if (data1 == 1) {
        DisplayAllProducts(data, data1)
    } else {
        DisplayAllProducts(data, data1.rates[sel.value])
    }
}

$('#resetButton').on('click', () => {
    clearForm()

})

function clearForm() {
    $("#productType").val("Type");
    $("#productCompany").val("Company");
    $("#productSize").val("Size");
    $("#productPrice").val("Price")
    $("#productColor").val("Color")
    $("#currency").val("Select coin")
    selectType = "All"
    selectCompany = "All"
    selectSize = "All"
    selectPrice = "All"
    selectColor = "All"
}

function ShowFilteredProducts(products) {
    var Ptbl = $("#productTable");
    $("#productTable").find("tr:gt(0)").remove();
    for (i = 0; i < products.length; i++) {
        if (products[i].initialAmount != 0) {
            var newRow = "<tr id=" + products[i].productID + ">" +
                "<td class=prodID>" + products[i].productID + "</td>" +
                "<td class=prodtype>" + products[i].productType + "</td>" +
                "<td class=prodcompany>" + products[i].productCompany + "</td>" +
                "<td class=prodsize>" + products[i].size + "</td>" +
                "<td class=prodprice>" + products[i].price + "</td>" +
                "<td class=prodcolor>" + products[i].color + "</td>" +
                "<td class=prodinitialamount>" + products[i].initialAmount + "</td>" +
                "<td>" + "<input type=button value=select onclick=Buy(" + products[i].productID + ")>" + "</td>" +
                "</tr>";
            Ptbl.append(newRow);
        }
    }
    $("#status").text("");
}


function ChangeSelectedOption(input) {
    if (input.id == "productType") {
        selectType = input.value;
    }
    if (input.id == "productCompany") {
        selectCompany = input.value;
    }
    if (input.id == "productSize") {
        selectSize = input.value;
    }
    if (input.id == "productPrice") {
        selectPrice = input.value;
    }
    if (input.id == "productColor") {
        selectColor = input.value;
    }

    //unselected
    if (selectType == undefined) {
        selectType = "All";
    }
    if (selectCompany == undefined) {
        selectCompany = "All";
    }
    if (selectSize == undefined) {
        selectSize = "All";
    }
    if (selectPrice == undefined) {
        selectPrice = "All";
    }
    if (selectColor == undefined) {
        selectColor = "All";
    }
}