﻿$("#status").text("Loading.........");
//$.get("/Product/GetProductByJson", null, DisplayAllProducts);
$.ajax({
    type: "get",
    url: "/Product/GetProductByJson",
    success: function (data) {
        DisplayAllProducts(data, 1)
    }
})


var sel = document.getElementById('currency');

sel.addEventListener("change", GetCurrentCoin);

function GetCurrentCoin() {

    $.post("/Order/GetCurrentDollarJson", { "currCoin": sel.value }, GetCurrency);


}
function GetCurrency(currentCoin) {
    let data1 = JSON.parse(currentCoin)
    //$("#currentDollar").text(currentCoin);
    $.ajax({
        type: "get",
        url: "/Product/GetProductByJson",
        success: function (data) {
            if (data1 == 1) {
                DisplayAllProducts(data, data1)
            } else {
                DisplayAllProducts(data, data1.rates[sel.value])
            }
            
        }
    })
}

function DisplayAllProducts(products, currCoin) {
    var Ptbl = $("#productsView");
    var ptblTemp = $("#producttable");
    $("#producttable").find("tr:gt(0)").remove();
    var productsHtml = "";
    var productsTableHtml = "";
    for (i = 0; i < products.length; i++) {
        if (products[i].initialAmount != 0) {
            var newRow = "<tr id=" + products[i].productID + ">" +
                "<td class=prodID>" + products[i].productID + "</td>" +
                "<td class=prodtype>" + products[i].productType + "</td>" +
                "<td class=prodcompany>" + products[i].productCompany + "</td>" +
                "<td class=prodsize>" + products[i].size + "</td>" +
                "<td class=prodprice>" + (products[i].price * currCoin).toFixed(2) + (sel.value === "USD" ? "$" : sel.value === "JPY" ? "¥" : sel.value === "BGN" ? "лв" : "€") + "</td>" +
                "<td class=prodcolor>" + products[i].color + "</td>" +
                "<td class=prodinitialamount>" + products[i].initialAmount + "</td>" +
                "<td>" + "<input type=button value=select onclick=Buy(" + products[i].productID + ")>" + "</td>" +
                "</tr>";
            ptblTemp.append(newRow);

            productsHtml = productsHtml +
                "<div class='product-grid6'>" +
                "<div class='product-image6'>" +
                "<a href='#'>" +
                "<img height='100px' class='pic-1' src='/Content/img/Products/" + products[i].productID + ".png'>" +
                "</a>" +
                "</div>" +
                "<div class='product-content'>" +
                "<h3 class='title'><a href='#'>" + products[i].productCompany + " " + products[i].productType + "</a></h3>" +
                "<span class='size'>size:" + products[i].size + "       " +  "</span>" +
                "<div class='price'>" + (products[i].price * currCoin).toFixed(2) + (sel.value === "USD" ? "$" : sel.value === "JPY" ? "¥" : sel.value === "BGN" ? "лв" : "€") +
                "</div>" +
                "</div>" +
                "<ul class='social'>" +
                "<li><a  data-toggle='modal' data-target='#myModal' onclick=Buy(" + products[i].productID + ") data-tip='Order now'>" +
                "<img height='31px' style='margin-top:5px;' src='/Content/img/buyIcon.png'>" +
                "</a ></li > " +
                "</ul>" +
                "</div>";

        }
    }
    Ptbl.html(productsHtml);

    $("#status").text("");
}

function Buy(id) {

    var data = $("#" + id);
    $("#valID").val(data.find(".prodID").text());
    $("#valType").val(data.find(".prodtype").text());
    $("#valCompany").val(data.find(".prodcompany").text());
    $("#valSize").val(data.find(".prodsize").text());
    $("#valPrice").val(data.find(".prodprice").text());
    $("#valColor").val(data.find(".prodcolor").text());
    $("#valInitialAmount").val(data.find(".prodinitialamount").text());


    var Pselcet = $("#selectAmount");
    Pselcet.empty();
    var ammount = data.find(".prodinitialamount").text();
    for (i = 0; i < ammount; i++) {

        var newOption = "<option>" + eval(i + 1) + "</option>"
        Pselcet.append(newOption);
    }
}


function orderSuccess(d) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        background: 'green',
        timer: 3000
    })
    Toast.fire({
        type: 'success',
        title: "The order was placed successfully!"
    })

    $.ajax({
        type: "get",
        url: "/Product/GetProductByJson",
        success: function (data) {
            DisplayAllProducts(data, 1)
        }
    })
}
function ScrollUp() {
    $('html,body').animate({ scrollTop: 0 }, 1000);
}