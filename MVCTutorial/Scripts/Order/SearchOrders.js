var productTypes = ["shirt", "polo", "shorts", "pants", "sweater", "jacket", "dress", "skirts", "top", "shoes", "jeans"];
var productID = []
var selectID = "All";
var selectName = "All";
var supplierIDField = "";

GetProductsID().then(() => {
    CreateSelectOptions(productID, "ProductID");
});
CreateSelectOptions(productTypes, "ProductName");

async function GetProductsID() {
    await $.ajax({
        type: "GET",
        url: '/Product/GetProductByJson',
        success: function (products) {
            for (var product of products) {
                productID.push(product.productID)
            }
        }
    })
}
function ResetForm() {
    $('select#ProductID').val("Product ID")
    $('select#ProductName').val("Product Name")
    $('#suplier-id-field').val("");
    selectID = "All";
    selectName = "All";
    supplierIDField = "";
}

function ResetInput() {
    $('#suplier-id-field').val("");
    $('#suplier-id-field').focus();
}
function ChangeSelectedOption(input) {
    if (input.id == "ProductID") {
        selectID = input.value;
    }
    if (input.id == "ProductName") {
        selectName = input.value;
    }
    if (selectID == undefined) {
        selectID = "All";
    }
    if (selectName == undefined) {
        selectName = "All";
    }
}

function Search() {
    supplierIDField = $("#suplier-id-field").get()[0].value;
    $.ajax({
        type: "POST",
        url: '/Order/SearchOrder',
        data: JSON.stringify({ productID: selectID, productName: selectName, supplierID: supplierIDField }),
        contentType: 'application/json; charset=utf-8',
        success: DisplayOrders
    })

}
