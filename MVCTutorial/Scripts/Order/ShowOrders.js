﻿
$("#status").text("Loading.........");
$.get("GetOrderByJson", null, DisplayOrders);

function DisplayOrders(orders) {
    if (orders == null || orders.length == 0) { //no orders arrived -show the no order div
        $("thead").hide();
        $(".no-orders-div").removeClass("hidden")
    }
    else {
        $("thead").show();
        $(".no-orders-div").addClass("hidden")
    }
    var Ptbl = $("#orderTable");
    $("#orderTable").find("tr").remove();

    for (i = 0; i < orders.length; i++) {
        if (orders[i].order != null) {
            var newRow = "<tr id=" + orders[i].order.orderID + ">" +
                "<td class=ordid>" + orders[i].order.orderID + "</td>" +
                "<td class=ordprodid>" + orders[i].order.productID + "</td>" +
                "<td class=ordname>" + orders[i].product.productType + "</td>" +
                "<td class=ordamount>" + orders[i].order.amount + "</td>" +
                "<td class=ordname>" + orders[i].order.supplierID + "</td>" +
                "<td class=edit-order>" + "<i class='fas fa-edit fa-lg icon-btn' onclick='EditOrderAmount(" + orders[i].order.productID + "," + orders[i].order.orderID + "," + orders[i].order.amount + ")'</i>" + "</td>" +
                "<td class=delete-order>" + "<i class='fas fa-trash-alt fa-lg icon-btn' onclick='ConfirmDelete(" + orders[i].order.orderID + ")'</i>" + "</td>" +
                "</tr>";
            Ptbl.append(newRow);
        }

    }
    $("#status").text("");
}

function ConfirmDelete(id) {
    Swal.fire({
        title: "Are you sure?",
        text: "The order will be deleted permenantly!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonColor: '#3085d6',
    }).then((result) => {
        if (result.value)
            DeleteOrder(id)
    })
}
function DeleteOrder(id) {
    var orderToDelete = JSON.stringify({ "ID": id });

    $.ajax({
        type: "DELETE",
        url: '/Order/DeleteOrderByJson',
        data: orderToDelete,
        contentType: 'application/json; charset=utf-8',
        success: DisplayOrders
    }).then(() => { DisplayToast("success", "Order deleted successfully!", "green") })


}

async function EditOrderAmount(orderedProductID, orderID, orderAmount) {
    var productDBAmount = await GetProductAmmountInTheStock(orderedProductID);
    if (productDBAmount == 0) {
        RunOutOfStock();
        return;
    }
    var newAmount = await OpenEditBox(productDBAmount);
    await UpdateDB(orderID, orderAmount, newAmount);
}
async function GetProductAmmountInTheStock(orderedProductID) {
    var productAmount = 0;
    await $.ajax({
        type: "GET",
        url: '/Product/GetProductByJson',
        success: function (products) {
            for (var product of products) {
                if (product.productID == orderedProductID) {
                    productAmount = product.initialAmount
                    return;
                }

            }
        }
    })
    return Promise.resolve(productAmount);
}
async function OpenEditBox(productMaxAmount) {
    var selectedAmount = 0;
    await Swal.fire({
        title: 'Select the desired amount',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        input: 'range',
        inputAttributes: {
            min: 1,
            max: productMaxAmount,
            step: 1
        },
        inputValue: Math.round(productMaxAmount / 2)
    }).then((value) => {
        selectedAmount = value
    })
    return Promise.resolve(selectedAmount.value);
}

async function UpdateDB(orderID, orderAmount, productNewAmount) {
    if (productNewAmount == null) return;
    await $.ajax({
        type: "PUT",
        url: '/Order/ChangeOrderAmount',
        cache: false,
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({ id: orderID, orderAmount: orderAmount, newAmount: productNewAmount }),
        success: DisplayOrders
    }).then(() => { DisplayToast("success", "Order edited successfully!", "green") })
}

function DisplayToast(status, message, backgroundColor) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        background: backgroundColor,
        timer: 3000
    })
    Toast.fire({
        type: status,
        title: message
    })
}
function RunOutOfStock() {
    Swal.fire({
        title: 'The product is out of stock',
        showConfirmButton: true,
    })
}

function ScrollUp() {
    document.body.scrollIntoView({ behavior: 'smooth', block: 'start' });
}
