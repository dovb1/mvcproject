﻿using System.Data.Entity;
using MVCTutorial.Models;
using System.Threading.Tasks;
using System.Text;

namespace MVCTutorial.Dal
{
    class DataLayer : DbContext
    {
        //internal object supliers;

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("tblUsers");
            modelBuilder.Entity<Product>().ToTable("tblProducts");
            modelBuilder.Entity<Order>().ToTable("tblOrders");
            modelBuilder.Entity<Location>().ToTable("tblLocations");
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Location> Locations { get; set; }
    }
}
