﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCTutorial.Models
{
    public class Location
    {
        [Required]
        public string title { get; set; }

        [Required]
        public string description { get; set; }

        [Key]
        [Required]
        public string coordinates { get; set; }
    }
}
