﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCTutorial.Models
{
    public class Order
    {

        [Key]
        [Required]
        public string orderID { get; set; }

        [Required]
        public string productID { get; set; }

        //[Required]
        
        //public string productName { get; set; }

        [Required]
        public int amount { get; set; }

        [Required]
        public string supplierID { get; set; }

        public string orderDate { get; set; }


    }
}