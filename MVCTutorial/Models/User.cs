﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCTutorial.Models
{
    public class User
    {
        [Key]
        [Required(ErrorMessage = "* UserId field required")]
        [RegularExpression("^[0-9]{9}$", ErrorMessage = "* User ID has to be exactly 9 digits")]
        public string userID { get; set; }

        [Required(ErrorMessage = "* Name field required")]
        [StringLength(10, ErrorMessage = "* Name must be between 3 and 10 characters", MinimumLength = 3)]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "* Name field must contain only letters")]
        public string name { get; set; }

        [Required(ErrorMessage = "* Password field required")]
        [StringLength(10, ErrorMessage = "* Password must be between 4 and 10 characters", MinimumLength = 4)]
        [RegularExpression("^[A-Za-z0-9]+$", ErrorMessage = "* Password field must contain digits and letters")]
        public string password { get; set; }
        public int permission { get; set; }

        //[Required(ErrorMessage = "* Confirm Password is required")]
        //[DataType(DataType.Password)]
        //[Compare("password",ErrorMessage ="Passwords dont match")]
        //public string confirmPassword { get; set; }

    }
}