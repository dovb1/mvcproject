﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCTutorial.Models
{
    public class Product
    {
        [Required]
        public string productID { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "* Product name must contain letters")]
        public string productType { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z' '&]+$", ErrorMessage = "* Product company must contain letters")]
        public string productCompany { get; set; }

        [Required]
        [RegularExpression("^[0-9]{2}$", ErrorMessage = "* Size must be two digits")]
        public int size { get; set; }

        [Required]
        [RegularExpression("^[0-9]+$", ErrorMessage = "* Price must be a number")]
        public int price { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "* Color must contains letters")]
        public string color { get; set; }

        [Required]
        [RegularExpression("^[0-9]+$", ErrorMessage = "* Amount must contain digits")]
        public int initialAmount { get; set; }

    }
}