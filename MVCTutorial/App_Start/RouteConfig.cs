﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVCTutorial
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            

            routes.MapRoute(
               name: "DefaultManagerPage",
               url: "Manager/ShowManagerPage",
               defaults: new { controller = "Manager", action = "ShowManagerPage", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "DefaultBranchPage",
               url: "Branch/ShowMap",
               defaults: new { controller = "Branch", action = "ShowMap", id = UrlParameter.Optional }
           );

            routes.MapRoute(
              name: "DefaultSuplierPage",
              url: "Supplier/ShowSupplierPage",
              defaults: new { controller = "Supplier", action = "ShowSupplierPage", id = UrlParameter.Optional }
          );

            routes.MapRoute(
                       name: "default",
                       url: "{controller}/{action}/{id}",
                       defaults: new { controller = "Home", action = "ShowHomePage", id = UrlParameter.Optional }
                   );

            //routes.MapRoute(
            //        "CatchAll",
            //        "{*anything}",
            //        new { controller = "Home", action = "ShowHomePage" }
            //);


        }
    }
}
