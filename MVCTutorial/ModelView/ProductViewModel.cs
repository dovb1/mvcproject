﻿using MVCTutorial.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTutorial.ViewModel
{
    public class ProductViewModel
    {
        public Product product { get; set; }
        public List<Product> products { get; set; }
    }
}