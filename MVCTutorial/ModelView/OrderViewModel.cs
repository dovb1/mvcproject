﻿using MVCTutorial.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTutorial.ViewModel
{
    public class OrderViewModel
    {
        public Order order { get; set; }
        public List<Order> orders { get; set; }
    }
}