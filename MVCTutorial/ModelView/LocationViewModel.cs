﻿using MVCTutorial.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTutorial.ModelView
{
    public class LocationViewModel
    {
        public Location location { get; set; }
        public List<Location> locations { get; set; }
    }
}
