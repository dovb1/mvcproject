﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVCTutorial.Models;

namespace MVCTutorial.ViewModel
{
    public class UserViewModel
    {
        public Product product{ get; set; }
        public List<Product> products{ get; set; }
        public User user { get; set; }
        public List<User> users { get; set; }

    }
}