﻿using MVCTutorial.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTutorial.ViewModel
{
    public class OrderProductViewModel
    {
        public Order order { get; set; }
        public Product product { get; set; }
    }
}