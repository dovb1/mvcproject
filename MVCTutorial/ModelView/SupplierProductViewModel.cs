﻿using MVCTutorial.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCTutorial.ViewModel
{
    public class SupplierProductViewModel
    {
        public int price { get; set; }
        public String productType { get; set; }
        public String productCompany { get; set; }
        public String supplierID { get; set; }
    }
}